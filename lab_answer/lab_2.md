1. Apakah perbedaan antara JSON dan XML?
Jika dilihat dari definisi, JSON dan XML merupakan dua hal yang berbeda.JSON adalah singkatan dari JavaScript Object Notation. JSON merupakan sebuah format file yang digunakan untuk membuat konten dalam suatu website. JSON menggunakan human readable text untuk menyimpan dan mentransmit data objek. XML adalah singkatan dari Extensible Markup Language yang digunakan untuk menyimpan dan mentransfer data.
Karena XML adalah markup language, tidak seperti JSON, XML dapat menampilkan data. Dari segi struktur dan bentuk kode, JSON hanya menggunakan tag pada bagian awal sedangkan XML pada bagian awal dan akhir. Tipe data dalam JSON dapat berupa string, integer, array, maupun boolean sedangkan XML hanya dapat berupa string. Dari segi keamanan, XML lebih aman jika dibandingkan dengan JSON.


2. Apakah perbedaan antara HTML dan XML?
XML adalah singkatan dari Extensible Markup Language. HTML adalah singkatan dari Hypertext Markup Language. HTML merupakan sebuah markup languange yang digunakan untuk menampilkan data pada halaman webstite dan aplikasi. Oleh karena itu, dapat dikatakan bahwa fokus utama dari HTML adalah penampilan dari data. XML merupakan framework yang digunakan mendefinisikan markup language untuk mentranspor dan menyimpan data pada database. HTML bersifat static sedangkan XML dynamic. XML bersifat case sensitive sedangkan HTML tidak. Pada HTML dapat tetap berjalan dengan error yang bersifat minor sedangkan XML tidak. Dari segi struktur dan bentuk kode, menggunakan tag pada bagian akhir pada XML ada suatu keharusan sedangkan HTML tidak.

Referensi:
https://www.guru99.com/json-vs-xml-difference.html#7
https://askanydifference.com/id/perbedaan-antara-json-dan-xml/
https://www.javatpoint.com/html-vs-xml
https://dosenit.com/kuliah-it/pemrograman/perbedaan-xml-dan-html