import 'package:flutter/material.dart';

//sumber: https://blog.waldo.io/flutter-card/
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Eduspace Courses',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'Eduspace Courses'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blue,
          title: Text(widget.title),
        ),
        body: Container(
          padding: EdgeInsets.all(16.0),
          child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                children: [
                  buildCard('Math', 'By: Pavita'),
                  buildCard('Science', 'By: Ipul'),
                  buildCard('Geography', 'By: Sarah'),
                  buildCard('Psychology', 'By: Lala'),
                  buildCard('Computer', 'By: Cinta'),
                  buildCard('Culinary', 'By: Ika'),
                  buildCard('Chemistry', 'By: Sandra'),
                ],
              )),
        ));
  }

  Card buildCard(title, subtitle) {
    return Card(
        color: Colors.lightBlue[100],
        elevation: 8.0,
        child: Column(
          children: [
            ListTile(
              title: Text(title),
              subtitle: Text(subtitle),
              trailing: Icon(Icons.favorite_outline),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              alignment: Alignment.centerLeft,
            ),
            ButtonBar(
              children: [
                TextButton(
                  child: const Text('ENROL'),
                  onPressed: () {/* ... */},
                ),
                TextButton(
                  child: const Text('LEARN MORE'),
                  onPressed: () {/* ... */},
                )
              ],
            )
          ],
        ));
  }
}
