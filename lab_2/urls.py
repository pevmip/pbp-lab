from django.urls import path
from .views import index
from .views import xml
from .views import json

urlpatterns = [
    path('', index, name='index'),
    path('json', json),
    path('xml', xml),
]
